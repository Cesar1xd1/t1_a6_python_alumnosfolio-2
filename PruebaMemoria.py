'''
Created on 2 oct. 2020

@author: Cesar
'''
class Alumno:
    
    def __init__(self, folio, nombre, edad, rs):
        if folio is None:
            self.__folio = int()
        else:
            self.__folio = folio
        
        if nombre is None:
            self.__nombre = str("")
        else:
            self.__nombre = nombre
        
        if edad is None:
            self.__edad = 0
        else:
            self.__edad = edad
        
        if rs is None:
            self.__rs = []
        else:
            self.__rs = rs
    
    def getFolio(self):
        return self.__folio
    def setFolio(self, folio):
        self.__folio=folio
    
    def getNombre(self):
        return self.__nombre
    def setNombre(self, nombre):
        self.__nombre=nombre
    
    def getEdad(self):
        return self.__edad
    def setEdad(self, edad):
        self.__edad=edad
    
    def getRs(self):
        return self.__rs
    def setRs(self, rs):
        self.__rs=rs
    
    def mostrar(self):
        print("folio : "+str(self.getFolio())+", Nombre: "+self.getNombre()+", Edad: "+str(self.getEdad())+", Redes Sociales: [", end="")
        print(*self.getRs(), sep = ", ", end = "]")
    

class RegistroDeAlumnos:
    
    
    
    def __init__(self, listaA, numFol):
        if listaA is None:
            self.__listaA = []
        else:
            self.__listaA = listaA
        
        if numFol is None:
            self.__numFol = 1
        else:
            self.__numFol = numFol
        
    
    def getlistaA(self):
        return self.__listaA
    
    def setListaA(self, listaA):
        self.__listaA=listaA
    
    def getNumFol(self):
        return self.__numFol
    
    def setNumFol(self, numFol):
        self.__numFol=numFol
    
    def correccion(self):
        e=1
        while e==1:
            try:
                r = int(input())
            except:
                print("Ups! por favor solo numeros mayores a 0")
                e=1
            else:
                if r>0:
                    e=0
                else:
                    print("Ups! por favor solo numeros mayores a 0")
                    e=1
        return r
    
    
    def agregar(self, al):
        self.setNumFol(self.getNumFol()+1)
        Alumno = self.getlistaA()
        
        Alumno.append(al)
        self.setListaA(Alumno)
    
    def eliminar(self, folio):
        self.setNumFol(self.getNumFol()-1)
        Alumno = self.getlistaA()
        
        Alumno.pop()
        self.setListaAspirantes(Alumno)
        
    def mostrar(self):
        
        ele = self.getlistaA()
        print("[ ", end="")
        for x in ele:
            Alumno.mostrar(x)
            
            print()
        print("]", end="")
        print()
    
regAlu = RegistroDeAlumnos([],1)
nombre = ""
folio = ""
opcion = 0
edad = 0

while opcion!=4:
    print("Digite 1 para Agregar alumno")
    print("Digite 2 para Eliminar alumno")
    print("Digite 3 para Mostrar alumnos")
    print("Digite 4 para ***SALIR***")
    opcion = regAlu.correccion()
    
    if opcion==1:
        redes = []
        nombre=str(input("Ingrese el nombre del Alumno"))
        print("Ingresa la edad:")
        edad=regAlu.correccion()
        redes.append(input("Ingresa el Facebook:"))
        redes.append(input("Ingresa el Instragram:"))
        redes.append(input("Ingresa el Whats app:"))
        
        if len(regAlu.getlistaA())==0:
            regAlu.agregar(Alumno(1,nombre, edad, redes))
        else:
            regAlu.agregar(Alumno(regAlu.getNumFol(), nombre, edad, redes))
    elif opcion==2:
        regAlu.eliminar(folio)
    elif opcion==3:
        regAlu.mostrar()
    elif opcion==4:
        print("Gracias por usar le Programa, usted a salido")
        pass
    else:
        print("Ups! opcion invalida, intenta otra vez")


    
    